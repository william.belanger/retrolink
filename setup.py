#!/usr/bin/python3
import os
import setuptools
import setuptools.command.build_py

here = os.path.abspath(os.path.dirname(__file__))


class CreateDesktopFile(setuptools.command.build_py.build_py):
    def run(self):
        with open(os.path.join(here + "/retrolink.desktop"), 'w') as f:
            f.write("[Desktop Entry]\n")
            f.write("Name=Retrolink\n")
            f.write("Type=Application\n")
            f.write("Categories=Game;\n")
            f.write("Icon=retrolink\n")
            f.write("Exec=retrolink\n")

        with open(os.path.join(here + "/retrolink-config.desktop"), 'w') as f:
            f.write("[Desktop Entry]\n")
            f.write("Name=Retrolink (configuration)\n")
            f.write("Type=Application\n")
            f.write("Categories=Settings;\n")
            f.write("Icon=retrolink\n")
            f.write("Exec=retrolink --config\n")
        setuptools.command.build_py.build_py.run(self)


install_requires = []
try:
    from PyQt5 import uic
    uic.compileUiDir('retrolink/ui')
except ImportError:
    install_requires.append("pyqt5")

setuptools.setup(
    name='retrolink',
    version='0.0.1',
    description='Seamless integration of frontend and emulators through gamepad hotkeys',
    keywords='gamepad hotkey emulator retroarch emulationstation',
    author='William Belanger',
    url='https://gitlab.com/william.belanger/retrolink',

    # From https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    cmdclass={'build_py': CreateDesktopFile},
    data_files=[
        ('share/icons/hicolor/scalable/apps', ['retrolink/ui/icons/retrolink.svg']),
        ('share/applications', ['retrolink.desktop']),
        ('share/applications', ['retrolink-config.desktop'])
    ],
    packages=setuptools.find_packages(),
    install_requires=install_requires,
    entry_points={
        'console_scripts': [
            'retrolink=retrolink:main',
        ],
    },
)
