# Retrolink
Retrolink fill the gap between the frontend (ie. EmulationStation) and the emulators. It allows binding a key combination on any Bluetooth gamepad, which then toggle either the frontend or the emulators. Therefore, both can then be opened and closed without a keyboard, solely by using a wireless controller. The wrapper also provides a black background to avoid the flickering during the transitions. Retrolink consist of two parts;

**Configuration GUI**
- Allows binding hotkeys to multiple Bluetooth gamepads
- Provides a script to manage permanent symlinks for these devices
- Run custom scripts before or after the execution of the frontend

![alt tag](https://gitlab.com/william.belanger/retrolink/raw/master/screenshots/configuration.gif)

**Evdev Daemon**
- Run in the background and listen for matching evdev events
- Toggle the frontend or the emulators, as needed
- Auto disconnects the gamepads on exit

![alt tag](https://gitlab.com/william.belanger/retrolink/raw/master/screenshots/daemon.gif)

**Command line interface**
```
--help, -h: Show this help message
--no-init: Only pass the command to an existing instance
--quit: Close the application
--config: Open the configuration GUI
```

# extra/wrapper.py
Assist the launch of standalone games from the emulator frontend
- Take ownership of the game subprocess and it's wrappers; killing wrapper.py closes its children
- Set the SDL\_GAMECONTROLLERCONFIG variable for most common (235) Linux gamepads
- Set the current working directory
- Set custom environment variables
- Launch processes before, after, or along with the main program
- Ideal for games lacking gamepad support

**Command line interface**
```
--help, -h: Show this help message
--run: Launch the main program (full path required)
--env: Set custom environment variables (ie. "VAR=value")
--wrap: Launch non-blocking processes and terminate them along with the main program
--before: Execute blocking processes before the main program
--after: Execute blocking processes after the main program
```

**Launcher example**
```
#!/bin/bash
# ~/.emulationstation/collections/pc/Elastomania.sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/.games/wrapper.py --run "$DIR/.games/Elastomania/eol2" --env "LD_LIBRARY_PATH=./lib" --wrap "antimicro --profile $DIR/.antimicro/Elastomania.amgp"
```

# Installation
- Arch Linux: install 'retrolink-git' from the AUR
