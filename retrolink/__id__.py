ID = "retrolink"
HELP = ["--help, -h: Show this help message",
        "--quit: Close the application",
        "--no-init: Only pass the command to an existing instance",
        "--config: Open the configuration GUI"]
