#!/usr/bin/python3
import os
import sys
import shlex
import signal
import subprocess


def blocking(process_list):
    """ Launch blocking processes before and/or after the main program """
    for p in process_list:
        try:
            subprocess.run(shlex.split(p))
        except FileNotFoundError:
            print(f"Init process '{p}' not found")


def environ(cmd):
    """ Add common controllers mapping and custom environment variables """
    env = os.environ.copy()
    local = os.path.dirname(os.path.realpath(__file__))
    try:
        with open(f"{local}/sdl_linux.txt") as f:
            env["SDL_GAMECONTROLLERCONFIG"] = f.read()
    except FileNotFoundError:
        print("Could not set SDL_GAMECONTROLLERCONFIG (db not found)")

    for v in cmd.get("env", []):
        v = v.split("=")
        env[v[0]] = v[1]
    return env


def kill(sig, frame, pids):
    """ Kill the game and the wraps on exit (ie. killall wrapper.py)"""
    for p in pids:
        try:
            os.killpg(p, signal.SIGTERM)
        except ProcessLookupError:
            pass


def parse(sysArgv):
    """ Convert sys.argv list to a dict of commands and arguments """
    cmd, parsed = None, {}
    for arg in sysArgv:
        if arg.startswith("--"):
            cmd = arg[2:]
            parsed.setdefault(cmd, [])
        elif cmd:
            parsed[cmd].append(arg)
    return parsed


def run(cmd, env):
    """ Set the current working dir, the environment, the hooks. Do all the subprocess calls """
    path = cmd["run"][0]
    cwd = os.path.dirname(path)
    path = "./" + os.path.basename(path)

    if not cwd:
        print("--run require a full executable path")
    else:
        process_list = cmd.get("before", [])
        blocking(process_list)

        pids = []
        process_list = cmd.get("wrap", [])
        for process in wrap(process_list):
            pids.append(process.pid)
        game = subprocess.Popen(path, env=env, cwd=cwd, preexec_fn=os.setsid)
        pids.append(game.pid)

        hook = lambda sig, frame: kill(sig, frame, pids)
        signal.signal(signal.SIGTERM, hook)
        signal.signal(signal.SIGABRT, hook)
        signal.signal(signal.SIGINT, hook)
        game.wait()
        hook(None, None)

        process_list = cmd.get("after", [])
        blocking(process_list)


def wrap(process_list):
    """ Launch non-blocking processes before the main program, yield the process to connect a kill hook """
    for p in process_list:
        try:
            process = subprocess.Popen(shlex.split(p), preexec_fn=os.setsid)
            yield process
        except FileNotFoundError:
            print(f"Wrapper process '{p}' not found")


cmd = parse(sys.argv[1:])
help_switch = bool("help" in cmd or "h" in cmd)

if help_switch or not cmd.get("run"):
    help_msg = [
        "--help, -h: Show this help message",
        "--run: Launch the main program (full path required)",
        "\nOptional parameters:",
        '--env: Set custom environment variables (ie. "VAR=value")',
        "--wrap: Launch non-blocking and terminate them along with the main program",
        "--before: Execute blocking processes before the main program",
        "--after: Execute blocking processes after the main program",
    ]
    for h in help_msg:
        print(h)
else:
    env = environ(cmd)
    run(cmd, env)
sys.exit()
